#!/usr/bin/python
#-*- coding: utf-8 -*-

import numpy as np
import threading
import time
from datetime import datetime
import jderobot
import math
import cv2
from math import pi as pi
import random

time_cycle = 80
spiral_value = 0
change_motion_flag = 10
collision_limit = 0

class MyAlgorithm(threading.Thread):

    def __init__(self, pose3d, motors, laser, bumper):
        self.pose3d = pose3d
        self.motors = motors
        self.laser = laser
        self.bumper = bumper

        self.stop_event = threading.Event()
        self.kill_event = threading.Event()
        self.lock = threading.Lock()
        threading.Thread.__init__(self, args=self.stop_event)


    def parse_laser_data(self,laser_data):
        laser = []
        for i in range(180):
            dist = laser_data.values[i]
            angle = math.radians(i)
            laser += [(dist, angle)]
        return laser

    def laser_vector(self,laser_array):
        laser_vectorized = []
        for d,a in laser_array:
            x = d * math.cos(a) * -1
            y = d * math.sin(a) * -1
            v = (x, y)
            laser_vectorized += [v]
        return laser_vectorized

    def run (self):
        while (not self.kill_event.is_set()):

            start_time = datetime.now()

            if not self.stop_event.is_set():
                self.execute()

            finish_Time = datetime.now()

            dt = finish_Time - start_time
            ms = (dt.days * 24 * 60 * 60 + dt.seconds) * 1000 + dt.microseconds / 1000.0
            #print (ms)
            if (ms < time_cycle):
                time.sleep((time_cycle - ms) / 1000.0)

    def stop (self):
        self.motors.sendV(0)
        self.motors.sendAZ(0)
        self.stop_event.set()

    def play (self):
        if self.is_alive():
            self.stop_event.clear()
        else:
            self.start()

    def kill (self):
        self.kill_event.set()

    def laser_range_mean(self, start_value, end_value, is_actual_mean):
        value_sum = 0
        total = end_value - start_value
        laser_data = self.laser.getLaserData()
        laser_data = self.parse_laser_data(laser_data)
        laser_data = np.array(laser_data,dtype=float)
        for i in range (start_value,end_value):
            if laser_data[i][0] > 1 and not is_actual_mean:
                laser_data[i][0] = 1
            value_sum = value_sum + laser_data[i][0]
        return value_sum/total

    def spiral_motion(self):
        global spiral_value
        spiral_value = spiral_value + 1
        w = 2
        v = 0.6*math.exp(spiral_value*0.01)
        self.motors.sendW(-w)
        self.motors.sendV(v)

    def rotate_motion(self, rotate_duration, angular_vel):
        self.motors.sendVX(0)
        self.motors.sendAZ(angular_vel)
        time.sleep(rotate_duration)
        self.motors.sendAZ(0)
        time.sleep(0.3)

    def move_forward_motion(self, linier_vel):
        self.motors.sendW(0)
        self.motors.sendV(linier_vel)
    
    def move_forward_and_rotate_motion(self, rotate_duration, angular_vel, linier_vel):
        self.motors.sendVX(0)
        self.motors.sendAZ(angular_vel)
        self.motors.sendVX(linier_vel)
        time.sleep(rotate_duration)
        self.motors.sendVX(0)
        self.motors.sendAZ(0)

    def execute(self):
        print('Execute')
        ANGULAR_VEL_MAX = 3
        LINIER_VEL_MAX = 1
        ROTATE_TIME = np.random.uniform(0.2, 0.6)
        global spiral_value
        global change_motion_flag
        global collision_limit

        right_mean = self.laser_range_mean(0, 65, False)
        middle_mean = self.laser_range_mean(70, 110, False)
        left_mean = self.laser_range_mean(115, 180, False)

        if self.bumper.getBumperData().state == 0:
            collision_limit = 0
            if right_mean < 0.4 and left_mean < 0.4 and middle_mean < 0.3:
                spiral_value = 0
                change_motion_flag = 0
                self.move_forward_motion(-LINIER_VEL_MAX)
                time.sleep(1)

            elif middle_mean < 0.4:
                spiral_value = 0
                change_motion_flag = 0
                if right_mean > left_mean:
                    self.rotate_motion(ROTATE_TIME, ANGULAR_VEL_MAX)
                else:
                    self.rotate_motion(ROTATE_TIME, -ANGULAR_VEL_MAX)

            elif right_mean < 0.6:
                spiral_value = 0
                change_motion_flag = 0
                if right_mean < 0.3:
                    self.rotate_motion(1, -0.5)
                else:
                    self.move_forward_and_rotate_motion(
                        0.35, 1, LINIER_VEL_MAX)

            elif left_mean < 0.6:
                spiral_value = 0
                change_motion_flag = 0
                if left_mean < 0.3:
                    self.rotate_motion(1, 0.5)
                else:
                    self.move_forward_and_rotate_motion(
                        0.35, -1, LINIER_VEL_MAX)


            else:
                if change_motion_flag < 10:
                    self.move_forward_motion(LINIER_VEL_MAX)
                    time.sleep(0.4)
                    change_motion_flag += 1
                else:
                    self.spiral_motion()

        else:
            spiral_value = 0
            change_motion_flag = 0
            collision_limit += 1
            
            if right_mean > left_mean:
                self.rotate_motion(ROTATE_TIME, 2*ANGULAR_VEL_MAX)
                if right_mean > 0.4:
                    self.move_forward_and_rotate_motion(0.35, -1, LINIER_VEL_MAX)

            elif right_mean < left_mean:
                self.rotate_motion(ROTATE_TIME, -2*ANGULAR_VEL_MAX)
                if left_mean > 0.4:
                    self.move_forward_and_rotate_motion(0.35, 1, LINIER_VEL_MAX)

            if collision_limit > 10:
                collision_limit = 0
                self.move_forward_motion(1)
                time.sleep(3)

